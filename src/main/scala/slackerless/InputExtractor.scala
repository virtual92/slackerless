package slackerless

import java.util.{Map => JavaMap}

object InputExtractor {

  def extractStage(input: collection.Map[String, Object]): String = {
    input.get("requestContext").asInstanceOf[Some[JavaMap[String, Object]]].get.get("stage").asInstanceOf[String]
  }

}
