package slackerless

import java.util.{Map => JavaMap}

import scala.beans.BeanProperty

class ApiResponse(
                   @BeanProperty val body: String,
                   @BeanProperty val statusCode: Int,
                   @BeanProperty val headers: JavaMap[String, String],
                   @BeanProperty val isBase64Encoded: Boolean = false)
