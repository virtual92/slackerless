package slackerless

import com.amazonaws.services.s3.{AmazonS3, AmazonS3ClientBuilder}
import com.fasterxml.jackson.databind.{ObjectMapper, SerializationFeature}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.typesafe.config.ConfigFactory

class Config(bucket: String) {

  val aws: AmazonS3 = AmazonS3ClientBuilder.defaultClient()

  val config: com.typesafe.config.Config = {
    val str = aws.getObjectAsString(bucket, "app.conf")
    ConfigFactory.parseString(str)
  }

  val mapper: ObjectMapper = {
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    mapper.enable(SerializationFeature.INDENT_OUTPUT)
    mapper
  }

}

object Config {
  var config: Config = _

  def apply(): Config = config
}