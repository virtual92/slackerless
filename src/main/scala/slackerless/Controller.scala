package slackerless

import com.amazonaws.services.lambda.runtime.{Context, RequestHandler}
import java.util.{Map => JavaMap}
import collection.JavaConverters._
import com.typesafe.scalalogging.LazyLogging

abstract class Controller extends RequestHandler[JavaMap[String, Object], ApiResponse] with LazyLogging {

  override def handleRequest(input: JavaMap[String, Object], context: Context): ApiResponse = {
    val stage = InputExtractor.extractStage(input.asScala)
    Config.config = new Config(s"slackerless--$stage")

    val body = handle(input.asScala, context)
    val jsoned = Config().mapper.writeValueAsString(body)
    logger.info(s"Responding with $jsoned")
    new ApiResponse(jsoned, 200, Map[String, String]().asJava)
  }

  def handle(input: collection.Map[String, Object], context: Context): AnyRef


}
